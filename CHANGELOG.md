4.0.0 / 2023-09-07
==================

  * Generar les imatges en format linux/amd64,linux/arm64,linux/arm/v7

3.0.0 / 2022-11-15
==================

  * Versió php:8.1.12

2.3.0 / 2022-02-04
==================

  * Afegida arquitectura linux/arm/v8

2.2.2 / 2021-01-14
==================

  * Afegit script yii-wait-for-db

2.2.1 / 2021-01-11
==================

  * Fix. The SCRAM_SHA_256 authentication mechanism requires libmongoc built with ENABLE_SSL

2.2.0 / 2021-01-08
==================

  * Versió php:7.4.14
  * Eliminades dependències hirak/prestissimo i fxp/composer-asset-plugin

2.1.2 / 2020-11-17
==================

  * Creació de docker multiplataforma

2.1.1 / 2020-10-21
==================

  * Refactored Dockerfile
  * Fix. Totes les imatges es generen per arm
  * Fix. Canviar arm32v6 per arm32v7 al fer el build

2.1.0 / 2020-07-30
==================

  * Canviar imatge docker de debian a php

2.0.0 / 2020-07-23
==================

  * PHP 7.4

1.4.1 / 2020-01-24
==================

  * Evitar l'error en cas el fitxer no existeixi

1.4.0 / 2019-06-18
==================

  * Valor per defecte de YII_MIGRATE=false
  * Valor per defecte de YII_INSTALL=false
  * Valor per defecte de YII_DB_TIMEOUT=10

1.3.0 / 2019-06-17
==================

  * Afegir script /usr/loca/bin/before-entrypoint

1.2.3 /2019-02-07
==================

  * Validar l'existència de ./yii abans d'executar les migracions

1.2.2 / 2019-01-16
==================

  * Modificar el temps d'espera a la connexió de la base de dades

1.2.1 / 2019-01-15
==================

  * Afegir valors per defecte a les variables d'entorn

1.2 / 2019-01-15
==================

   * Afegir modul php7-mongodb

1.1 / 2019-01-14
==================

   * Afegir modul php7-pdo_pgsql
   * Eliminar scripts de backup i restore.

1.0 / 2018-12-12
==================

   * Versió inicial
