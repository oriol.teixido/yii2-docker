FROM php:8.1.12-apache

ENV APACHE_USER www-data

RUN apt-get update \
    && apt-get upgrade -y \
    &&  apt-get install -y \
        git \
        unzip \
        sudo \
        netcat \
        libpq-dev \
        libmagickwand-dev \
        libssl-dev \
    && apt-get autoremove -y \
    && rm -rf /var/cache/apt/*

RUN curl -sS https://getcomposer.org/installer | php -- --filename=composer --install-dir=/usr/local/bin

RUN pecl install mongodb imagick \
    && docker-php-ext-enable mongodb imagick \
    && docker-php-ext-install pdo pdo_mysql pdo_pgsql gd intl \
    && echo "Installed modules" && php -m

COPY docker/ /

RUN a2enmod rewrite && a2dissite 000-default

EXPOSE 80

WORKDIR /app

ENTRYPOINT ["/usr/local/bin/entrypoint"]

CMD ["/usr/sbin/apache2ctl", "-DFOREGROUND"]
