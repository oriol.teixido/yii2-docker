# YII-DOCKER

Imatge base per l'execució d'aplicacions basades en el framework Yii2

# Configuració

Copiar l'aplicació Yii2 al directori *app/*.

Variables de configuració

  - YII_DB_HOST=<DATABASE_HOSTNAME>
  - YII_DB_PORT=<DATABASE_PORT>
  - YII_DEV=<true|false> (defaults "false")
  - YII_PRETTY_URL=<true|false> (defaults "true")
  - YII_INSTALL=<true|false> (defaults "false")
  - YII_CHOWN=<true|false> (defaults "true")
  - YII_MIGRATE=<true|false> (defaults "false")
  - YII_DB_TIMEOUT=<VALUE> (defaults "10")
