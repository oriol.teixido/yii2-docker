#!/bin/bash -e

VERSION=$1
if [ -z "$VERSION" ]; then
    VERSION=latest
fi

docker buildx build --push --platform linux/amd64,linux/arm64,linux/arm/v7 --tag oteixido/yii2-docker:${VERSION} .
